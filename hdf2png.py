import sys
import math

import h5py
import numpy as np
from PIL import Image

if len(sys.argv) < 2:
    print('hdf2png.py <inputFile> <outputImage>')
    exit(1)

inputfile = sys.argv[1]
outputimage = sys.argv[2]

hdf = h5py.File(inputfile,'r')
raw = hdf["/dataset1/data1/data"][:]
quantity = hdf["/dataset1/data1/what"].attrs["quantity"].decode('latin-1')
nodata = hdf["/dataset1/data1/what"].attrs["nodata"]
undetect = hdf["/dataset1/data1/what"].attrs["undetect"]

# Conversion to dBZ if necessary
dBZ = None
if quantity == "DBZH":
    dBZ = raw
if quantity == "ACRR":
    # to mm/h
    R = (raw*0.01)

    # mm/h to Z using inverse Marshall-Palmer formula
    Z = np.power(200. * R, 1.6)
    dBZ = np.log10(Z) * 10.

# Convert dBZ to KNMI pixel values, clamp to 0-254 range
PV = ( dBZ - -32 ) / 0.5
PV[PV<0] = 0
PV[PV>254] = 254
PV[raw==nodata] = 0
PV[raw==undetect] = 255

# Save image
img = Image.fromarray(PV.astype('uint8'))
img.save(outputimage, "PNG")
