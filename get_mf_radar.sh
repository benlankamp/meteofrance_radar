APPLICATION_ID={vul id}

# Get JWT token from Meteo-France
TOKEN=$(curl -k -X POST https://portail-api.meteofrance.fr/token \
    -d "grant_type=client_credentials" \
    -H "Authorization: Basic ${APPLICATION_ID}" | jq -r '.access_token')

# Get reflectivity product
curl -X 'GET' \
    'https://public-api.meteofrance.fr/public/DPRadar/v1/mosaiques/METROPOLE/observations/REFLECTIVITE/produit?maille=1000' \
    -H 'Accept: application/octet-stream+gzip' \
    -H "Authorization: Bearer ${TOKEN}" \
    --output latest_reflectivity.gz

# Unpack and rename
if [ -f "latest.gz" ]; then
    gunzip latest_reflectivity.gz
    mv latest_reflectivity latest_reflectivity.bufr
fi

# Get rainrate product
curl -X 'GET' \
    'https://public-api.meteofrance.fr/public/DPRadar/v1/mosaiques/METROPOLE/observations/LAME_D_EAU/produit?maille=500' \
    -H 'Accept: application/x-hdf' \
    -H "Authorization: Bearer ${TOKEN}" \
    --output latest_rainrate.h5

# Process products
./process_mf_reflectivity.sh
./process_mf_rainrate.sh