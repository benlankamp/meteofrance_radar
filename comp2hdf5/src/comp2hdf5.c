/* Some standard headers */ #include <stdlib.h>

#include <stdio.h>

#include <string.h>

#include <assert.h>

#include <time.h>

#include <float.h>

/* Interface definition of the OPERA coder/decoder */
#include "desc.h"

#include "bufr.h"

#include "bitio.h"

#include "rlenc.h"

#include "radar.h"

#include "compo.h"

#include "odim.h"

odim_comp_t odim_data; /* structure holding ODIM data */

unsigned char * dbl_to_uchar(double * in, size_t N) {
  unsigned char * out = malloc(sizeof(unsigned char) * N);
  size_t i = 0;

  for (i = 0; i < N; i++) {
     out[i] = in[i];
  }

  return out;
}

static int write_hdf5(odim_comp_t * od, char * file) {
    hid_t file_id, dataset_id, dataspace_id, root_id, group_id, attr_id, space_id; /* identifiers */
    herr_t status;
    int i, j;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(file, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    root_id = H5Gopen(file_id, "/");

    status = H5Acreatestring(root_id, "Conventions", Conventions);

    group_id = H5Gcreate(root_id, "what", 0);
    status = H5Acreatestring(group_id, "object", od -> what.object);
    status = H5Acreatestring(group_id, "version", od -> what.version);

    {
        char str[100];
        int i;
        char * p;
        struct tm * local = gmtime( & od -> what.nominal_time);
        sprintf(str, "%04d%02d%02d", local -> tm_year + 1900, local -> tm_mon + 1, local -> tm_mday);
        status = H5Acreatestring(group_id, "date", str);
        sprintf(str, "%02d%02d%02d", local -> tm_hour, local -> tm_min, local -> tm_sec);
        status = H5Acreatestring(group_id, "time", str);
    }
    status = H5Gclose(group_id);

    //fprintf(stderr, "Making HOW group...\n");

    group_id = H5Gcreate(root_id, "how", 0);
    {
        char str[3000];
        int i;
        char * p = str;

        //fprintf(stderr, "stations: %d\n", od -> what.nstations);

        for (i = 0; i < od -> what.nstations; ++i) {
            sprintf(p, "%s:%s", od -> what.source[i].identifier, od -> what.source[i].value);
            p += strlen(p);
            if (i < od -> what.nstations - 1) {
                sprintf(p, ",");
                ++p;
            }
        }
        if (od -> what.nstations) {
            status = H5Acreatestring(group_id, "nodes", str);
        } else {
            fprintf(stderr, "Warning: nodes missing\n");
        }
    }
    status = H5Gclose(group_id);

    group_id = H5Gcreate(root_id, "where", 0);

    attr_id = H5Acreate(group_id, "xsize", H5T_STD_I32BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_INT, & od -> where.ncols);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "ysize", H5T_STD_I32BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_INT, & od -> where.nrows);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "xscale", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> where.psizex);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "yscale", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> where.psizey);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "LL_lon", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> where.sw.lon);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "LL_lat", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> where.sw.lat);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "UL_lon", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> where.nw.lon);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "UL_lat", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> where.nw.lat);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "UR_lon", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> where.ne.lon);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "UR_lat", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> where.ne.lat);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "LR_lon", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> where.se.lon);
    status = H5Aclose(attr_id);

    attr_id = H5Acreate(group_id, "LR_lat", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
    status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> where.se.lat);
    status = H5Aclose(attr_id);

    attr_id = H5Acreatestring(group_id, "projdef", od -> where.projdef);

    status = H5Gclose(group_id);

    for (i = 0; i <= od -> nproducts; ++i) {
        hid_t g_id;
        hsize_t dims[2];
        char str[20];

        sprintf(str, "dataset%d", i + 1);
        group_id = H5Gcreate(root_id, str, 0);

        g_id = H5Gcreate(group_id, "what", 0);
        status = H5Acreatestring(g_id, "product", od -> datasets[i].dataset_what.product);
        status = H5Acreatestring(g_id, "prodpar", "CAPPI");
        status = H5Acreatestring(g_id, "quantity", od -> datasets[i].dataset_what.quantity);

        attr_id = H5Acreate(g_id, "gain", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
        status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> datasets[i].dataset_what.gain);
        status = H5Aclose(attr_id);

        attr_id = H5Acreate(g_id, "offset", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
        status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> datasets[i].dataset_what.offset);
        status = H5Aclose(attr_id);

        attr_id = H5Acreate(g_id, "nodata", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
        status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> datasets[i].dataset_what.nodata);
        status = H5Aclose(attr_id);

        attr_id = H5Acreate(g_id, "undetect", H5T_IEEE_F64BE, H5Screate(H5S_SCALAR), H5P_DEFAULT);
        status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, & od -> datasets[i].dataset_what.undetect);
        status = H5Aclose(attr_id);

        status = H5Gclose(g_id);

        dims[0] = od -> where.ncols;
        dims[1] = od -> where.nrows;

        {
            int j = 0; /* always for composites */
            hid_t plist, data_id;
            char str[20];
            hsize_t cdims[2];
            cdims[0] = dims[0] / 2;
            cdims[1] = dims[1] / 2;
            sprintf(str, "data%d", j + 1);

            if (od -> datasets[i].data) {
                hid_t g_id;
                data_id = H5Gcreate(group_id, str, 0);
                dataspace_id = H5Screate_simple(2, dims, NULL);

                plist = H5Pcreate(H5P_DATASET_CREATE);
                status = H5Pset_chunk(plist, 2, cdims);
                status = H5Pset_deflate(plist, 6);

                int elementcount = od -> where.ncols * od -> where.nrows;
                dataset_id = H5Dcreate(data_id, "data", H5T_NATIVE_UCHAR, dataspace_id, plist);
                unsigned char *data_uint = dbl_to_uchar( od -> datasets[i].data, elementcount );

                status = H5Dwrite(dataset_id, H5T_NATIVE_UCHAR, H5S_ALL, H5S_ALL, H5P_DEFAULT, data_uint);

                status = H5Acreatestring(dataset_id, "CLASS", "IMAGE");
                status = H5Acreatestring(dataset_id, "VERSION", "1.2");
                
                status = H5Dclose(dataset_id);
                status = H5Pclose(plist);
                status = H5Sclose(dataspace_id);

                status = H5Gclose(data_id);
            }
        }

        status = H5Gclose(group_id);
    }

    status = H5Gclose(root_id);

    /* Close the file. */
    status = H5Fclose(file_id);

    return 1;
}

static int count_100 = 0;
static char bufr[100];
static int bufr_char_0_29_205(varfl val, int ind) {
    bufr[count_100++] = val;
    return 1;
}

static int count_255 = 0;
static char bufr_255[255];
static int bufr_char_255(varfl val, int ind) {
    bufr_255[count_255++] = val;
    return 1;
}

// sections: 0 = none, 1 = stations, 2 = products
static int delay_factor = 0;
static int section = 0;
static int section1_k = 0;
static int section2_k = 0;
static int data_k = 0;

static int FXY = 0, prevFXY = 0;

//static int highest_refl = 0, highest_PV = 0;

static int our_callback(varfl val, int ind) {
    static bool check_delay_replicator = false;

    static int second_3_21_204 = 0;
    odim_comp_t * od = & odim_data; /* our global data structure */
    static radar_data_t b;
    static int ncorners = 0, nscans = 0;

    int PV;

    if (des[ind] -> id == SEQDESC) {
        /* get descriptor */
        dd * d = & (des[ind] -> seq -> d);
        varfl * vv;

        if( check_delay_replicator ) {
            if(bufr_check_fxy(d, 3, 1, 1)) {
                if( section == 0 ) {
                    section = 1;
                    section1_k = 0;

                    // stations in composite
                    od -> what.nstations = delay_factor;
                    od -> what.source = calloc(od -> what.nstations, sizeof(odim_station_t));
                }
                
            }

            check_delay_replicator = 0;
        }

        /* open array for values */
        bufrval_t * v = bufr_open_val_array();
        if (v == NULL) return 0;

        /* decode sequence to global array */
        if (!bufr_parse_out(des[ind] -> seq -> del, 0, des[ind] -> seq -> nel - 1,
                bufr_val_to_global, 0)) {
            bufr_close_val_array();
            return 0;
        }
        vv = v -> vals;

        /* Composite date and time */
        if (section == 0) {
            if (bufr_check_fxy(d, 3, 1, 11)) {
                // date
                int i = 0;
                b.meta.year = vv[i++];
                b.meta.month = vv[i++];
                b.meta.day = vv[i++];
            } else if (bufr_check_fxy(d, 3, 1, 13)) {
                // time
                int i = 0;
                b.meta.hour = vv[i++];
                b.meta.min = vv[i++];
                b.meta.sec = vv[i++];
            } else if (bufr_check_fxy(d, 3, 29, 192)) {
                // TODO projection details
            }
        }

        if( section == 1) {
            if(bufr_check_fxy(d, 3, 1, 1)) {    
                int i = 0;            
                int WMO_block = vv[i++];
                int WMO_station = vv[i++];

                if( section1_k < od -> what.nstations ) {
                    od -> what.source[section1_k].identifier = "WMO";
                    od -> what.source[section1_k].value = calloc(6, sizeof(unsigned char));
                    sprintf( od -> what.source[section1_k].value, "%02i%03i", WMO_block, WMO_station );
                }

                section1_k++;
            }
        }

        /* close the global value array */
        bufr_close_val_array();
    }
    /* element descriptor */
    else if (des[ind] -> id == ELDESC) {
        dd * d = & (des[ind] -> el -> d);

        FXY = d->f * 100000 + d->x * 1000 + d->y;

        // skip operation descriptors
        if( d->f == 2 )
        {
            return 1;
        }

        if (section == 0) {
            if (bufr_check_fxy(d, 0, 1, 99)) {
                char* text = calloc( 255, sizeof(char));
                count_255 = 0;
                bufr_parse_out(d, 0, 0, bufr_char_255, 0);
                for (int k = 0; k < count_255; ++k) {
                    text[k] = bufr_255[k];
                }
                fprintf(stderr, "Product definition: %s\n", text);
                return 1;
            } else if (bufr_check_fxy(d, 0, 1, 192)) {
                fprintf(stderr, "Indicator of composite: %i\n", (int)val);
                return 1;
            } else if (bufr_check_fxy(d, 0, 29, 1)) {
                od -> where.projdef = calloc(255, sizeof(char));
                switch((int)val) {
                    case 0:             
                        // Gnomonic
                        sprintf( od -> where.projdef, "'+proj=gnom +R=1 +lat_0=0 +lon_0=0" );
                        break;
                    case 1:
                        // Stereographic (true scale: 45NB)
                        sprintf( od -> where.projdef, "+proj=stere +lat_0=90 +lat_ts=45 +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +units=m +no_defs" );
                        break;
                    case 2:
                        // Lambert Conic Conformal (ETRS89-extended)
                        sprintf( od -> where.projdef, "+proj=lcc +lat_0=52 +lon_0=10 +lat_1=35 +lat_2=65 +x_0=4000000 +y_0=2800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs" );
                        break;
                    case 3:
                        // Mercator
                        sprintf( od -> where.projdef, "+proj=longlat +datum=WGS84 +no_defs +type=crs" );
                        break;
                    default:
                        sprintf( od -> where.projdef, "Unknown" );
                        break;
                }

                fprintf(stderr, "Projection type: %s\n", od -> where.projdef);
                return 1; 
            } else if (bufr_check_fxy(d, 0, 29, 2)) {
                od -> where.projgrid = calloc(255, sizeof(char));
                switch((int)val) {
                    case 0:                   
                        sprintf( od -> where.projgrid, "Cartesian" );
                        break;
                    case 1:
                        sprintf( od -> where.projgrid, "Polar" );
                        break;
                    case 2:
                        sprintf( od -> where.projgrid, "Other" );
                        break;
                    default:
                        sprintf( od -> where.projgrid, "Unknown" );
                        break;
                }

                fprintf(stderr, "Projection grid: %s\n", od -> where.projgrid);
                return 1;
            } else if (bufr_check_fxy(d, 0, 5, 33)) {
                od -> where.psizex = val;
            } else if (bufr_check_fxy(d, 0, 6, 33)) {
                od -> where.psizey = val;
            } else if (bufr_check_fxy(d, 0, 30, 21)) {               
                od -> where.ncols = (int)val;
            } else if (bufr_check_fxy(d, 0, 30, 22)) {
                od -> where.nrows = (int)val;
            } else if (bufr_check_fxy(d, 0, 30, 31)) {
                // Picture type, 1 = composite
                if (val != 1) {
                    fprintf(stderr, "Unknown product %d\n", val);
                }
                fprintf(stderr, "Product: COMP\n");
            }  else if (bufr_check_fxy(d, 0, 31, 1)) {
                // Delayed replicator, could be nr. of stations or nr. of products
                // Need to check what the next FXY is
                delay_factor = val;
                check_delay_replicator = 1;
            }  else if (bufr_check_fxy(d, 0, 31, 192)) {
                delay_factor = val;
                section = 2;
            }
        } else if(section == 1) {
            if(section1_k == od->what.nstations) section = 0;
        } else if(section == 2) {
            if (prevFXY != FXY) {
                data_k = 0;
            }

            int reference_value = strcmp(des[ind] -> el -> elname, "Reference value") == 0;
            int known_product = bufr_check_fxy(d, 0, 10, 2) ||
                bufr_check_fxy(d, 0, 21, 1) ||
                bufr_check_fxy(d, 0, 21, 120);
            
            if ( known_product && !reference_value ) {
                if(data_k == 0) {
                    fprintf( stderr, "\t...%s\n", des[ind] -> el -> unit);
                    
                    // set overall what
                    if( od->nproducts == -1) {
                        od -> what.object = "COMP";
                        od -> what.version = "COMP2HDF5 v1";
                        {
                            char * tz = set_fuseau("TZ=UTC");
                            struct tm local;
                            local.tm_year = b.meta.year - 1900;
                            local.tm_mon = b.meta.month - 1;
                            local.tm_mday = b.meta.day;
                            local.tm_hour = b.meta.hour;
                            local.tm_min = b.meta.min;
                            local.tm_sec = b.meta.sec;
                            local.tm_wday = 0;
                            local.tm_yday = 0;
                            local.tm_isdst = 0;
                            od -> what.nominal_time = mktime( & local);
                        }
                    }
                    
                    // reflectivity
                    od -> nproducts++;

                    if(od -> nproducts == 0) {
                        od -> datasets = calloc(1, sizeof(odim_comp_dataset_t));
                    } else {
                        od -> datasets = realloc(od -> datasets, (od -> nproducts+1) * sizeof(odim_comp_dataset_t));
                    }

                    od -> datasets[od -> nproducts].dataset_what.product = "COMP";

                    if( bufr_check_fxy( d, 0, 10, 2 ) ) {
                        od -> datasets[od -> nproducts].dataset_what.quantity = "HGHT";
                    } else if ( bufr_check_fxy( d, 0, 21, 1 ) ) {
                        od -> datasets[od -> nproducts].dataset_what.quantity = "DBZH";
                    } else if ( bufr_check_fxy( d, 0, 21, 120 ) ) {
                        od -> datasets[od -> nproducts].dataset_what.quantity = "POR";
                    } else {
                        od -> datasets[od -> nproducts].dataset_what.quantity = "NA";
                    }

                    // for KNMI pixel values: use gain = 0.5 and offset = -32
                    od -> datasets[od -> nproducts].dataset_what.gain = 1;
                    od -> datasets[od -> nproducts].dataset_what.offset = 0;
                    od -> datasets[od -> nproducts].dataset_what.nodata = 255;
                    od -> datasets[od -> nproducts].dataset_what.undetect = 0;

                    int size = od->where.ncols * od->where.nrows * sizeof(varfl);
                    od -> datasets[od -> nproducts].data = (varfl *)malloc(size);
                }

                // modify data with gain and offset
                PV = ( val - od -> datasets[od -> nproducts].dataset_what.offset ) / od -> datasets[od -> nproducts].dataset_what.gain;
                if(PV < od -> datasets[od -> nproducts].dataset_what.undetect) PV = od -> datasets[od -> nproducts].dataset_what.undetect;
                if(PV >= od -> datasets[od -> nproducts].dataset_what.nodata) PV = od -> datasets[od -> nproducts].dataset_what.nodata;

                od -> datasets[od -> nproducts].data[data_k] = PV;
                
                data_k++;
            }
        }

        prevFXY = FXY;
    }
    return 1;
}

/*===========================================================================
                                  Main program
  ===========================================================================*/
int main(int argc, char * argv[]) {
    char * usage = "usage: transcode [-v] [-d tabdir] input_file output_file\n";
    char * version = "transcode V2.3, 02-10-2008\n";

    char * table_dir = NULL; /* directory for BUFR tables */
    sect_1_t s1;
    bufr_t msg;

    /******* check command line parameter */

    while (argc > 1 && * argv[1] == '-') {
        if ( * (argv[1] + 1) == 'v')
            fprintf(stderr, "%s", version);
        else if ( * (argv[1] + 1) == 'd') {
            if (argc < 2) {
                fprintf(stderr, "Missing parameter for -d\n\n%s", usage);
                exit(EXIT_FAILURE);
            }
            table_dir = argv[2];
            argc--;
            argv++;
        } else {
            fprintf(stderr, "Invalid parameter %s\n\n%s", argv[1], usage);
            exit(EXIT_FAILURE);
        }
        argc--;
        argv++;
    }

    /******* Get input- and output-filenames from the command-line */
    if (argc != 3) {
        fprintf(stderr, "%s", usage);
        exit(EXIT_FAILURE);
    }

    if (!bufr_read_file( & msg, argv[1])) {
        fprintf(stderr, "FATAL: Unable to read the BUFR-message in %s!\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    /******* decode section 1 */
    fprintf(stderr, "Input file header:\n");
    if (!bufr_decode_sections01( & s1, & msg)) {
        fprintf(stderr, "FATAL: Unable to decode section 1\n");
        exit(EXIT_FAILURE);
    }
    header_dump( & s1);

    {
        char * tz = set_fuseau("TZ=UTC");
        struct tm local;
        local.tm_sec = s1.sec;
        local.tm_min = s1.min;
        local.tm_hour = s1.hour;
        local.tm_mday = s1.day;
        local.tm_mon = s1.mon - 1;
        local.tm_year = s1.year - 1900;
        local.tm_wday = 0;
        local.tm_yday = 0;
        local.tm_isdst = 0;

        odim_data.what.nominal_time = mktime( & local);
        set_fuseau(tz);
    }

    /* read descriptor tables */
    if (read_tables(table_dir, s1.vmtab, s1.vltab, s1.subcent, s1.gencent)) {
        fprintf(stderr, "FATAL: Unable to read tables\n");
        exit(EXIT_FAILURE);
    }

    /* decode data descriptor and data-section now */

    {
        int ok, desch, ndescs;
        dd * dds = NULL;

        odim_data.nproducts = -1;

        /* open bitstreams for section 3 and 4 */
        desch = bufr_open_descsec_r( & msg, NULL);
        ok = (desch >= 0);
        if (ok) ok = (bufr_open_datasect_r( & msg) >= 0);

        /* calculate number of data descriptors  */
        ndescs = bufr_get_ndescs( & msg);

        /* allocate memory and read data descriptors from bitstream */
        if (ok) ok = bufr_in_descsec( & dds, ndescs, desch);

        /* output data to our global data structure */
        if (ok) ok = bufr_parse_out(dds, 0, ndescs - 1, our_callback, 1);

        /* fill the HDF5 ODIM data structure */
        if (ok) write_hdf5( & odim_data, argv[2]);

        /* close bitstreams and free descriptor array */
        if (dds != (dd * ) NULL)
            free(dds);
        bufr_close_descsec_r(desch);
        bufr_close_datasect_r();
    }

    free_descs();
    exit(EXIT_SUCCESS);
}