/*-------------------------------------------------------------------------
    
    BUFRdump to dump contents of a BUFR file
    Copyright (C) 2024  Ben Lankamp

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
-------------------------------------------------------------------------    
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <float.h>

#include "desc.h"
#include "bufr.h"
#include "bitio.h"
#include "rlenc.h"

static void header_dump(sect_1_t *s1);
static int bufr_callback(varfl val, int ind);

int main(int argc, char * argv[]) {
    char * usage = "usage: bufrdump [-d tabdir] bufr_file\n";
    char * version = "BUFRdump V1.0, 17-01-2024\n";

    char * table_dir = NULL; /* directory for BUFR tables */
    sect_1_t s1;
    bufr_t msg;

    /******* check command line parameter */
    while (argc > 1 && * argv[1] == '-') {
        if ( * (argv[1] + 1) == 'v')
            fprintf(stderr, "%s", version);
        else if ( * (argv[1] + 1) == 'd') {
            if (argc < 2) {
                fprintf(stderr, "Missing parameter for -d\n\n%s", usage);
                exit(EXIT_FAILURE);
            }
            table_dir = argv[2];
            argc--;
            argv++;
        } else {
            fprintf(stderr, "Invalid parameter %s\n\n%s", argv[1], usage);
            exit(EXIT_FAILURE);
        }
        argc--;
        argv++;
    }

    if (argc != 2) {
        fprintf(stderr, "%s", usage);
        exit(EXIT_FAILURE);
    }

    if (!bufr_read_file( & msg, argv[1])) {
        fprintf(stderr, "FATAL: Unable to read the BUFR-message in %s!\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    /******* decode section 1 */
    fprintf(stderr, "Input file header:\n");
    if (!bufr_decode_sections01( & s1, & msg)) {
        fprintf(stderr, "FATAL: Unable to decode section 1\n");
        exit(EXIT_FAILURE);
    }
    header_dump( & s1 );

    /* read descriptor tables */
    if (read_tables(table_dir, s1.vmtab, s1.vltab, s1.subcent, s1.gencent)) {
        fprintf(stderr, "FATAL: Unable to read tables\n");
        exit(EXIT_FAILURE);
    }

    /* decode data descriptor and data-section now */
    {
        int ok, desch, ndescs;
        dd * dds = NULL;

        /* open bitstreams for section 3 and 4 */
        desch = bufr_open_descsec_r( & msg, NULL);
        ok = (desch >= 0);
        if (ok) ok = (bufr_open_datasect_r( & msg) >= 0);

        /* calculate number of data descriptors  */
        ndescs = bufr_get_ndescs( & msg);

        /* allocate memory and read data descriptors from bitstream */
        if (ok) ok = bufr_in_descsec( & dds, ndescs, desch);

        /* output data to our global data structure */
        if (ok) ok = bufr_parse_out(dds, 0, ndescs - 1, bufr_callback, 1);

        /* close bitstreams and free descriptor array */
        if (dds != (dd * ) NULL)
        {
            free(dds);
        }

        bufr_close_descsec_r(desch);
        bufr_close_datasect_r();
    }

    free_descs();
    exit(EXIT_SUCCESS);
}

static void header_dump(sect_1_t *s1)
{
    fprintf (stderr, "%5d    BUFR edition                       \n", _bufr_edition);
    fprintf (stderr, "%5d    master table used                  \n", s1->mtab);
    fprintf (stderr, "%5d    subcenter                          \n", s1->subcent);
    fprintf (stderr, "%5d    generating center                  \n", s1->gencent);
    fprintf (stderr, "%5d    original BUFR message              \n", s1->updsequ);
    fprintf (stderr, "%5d    no optional section                \n", s1->opsec);
    fprintf (stderr, "%5d    message type                       \n", s1->dcat);
    fprintf (stderr, "%5d    message subtype                    \n", s1->dcatst);
    fprintf (stderr, "%5d    international message subtype      \n", s1->idcatst);
    fprintf (stderr, "%5d    version number of master table used\n", s1->vmtab);
    fprintf (stderr, "%5d    version number of local table used \n", s1->vltab);
    fprintf (stderr, "%5d    year                               \n", s1->year);
    fprintf (stderr, "%5d    month                              \n", s1->mon);
    fprintf (stderr, "%5d    day                                \n", s1->day);
    fprintf (stderr, "%5d    hour                               \n", s1->hour);
    fprintf (stderr, "%5d    minute                             \n", s1->min);
    fprintf (stderr, "%5d    sec                                \n", s1->sec);

    return;
}

// for consuming strings up to 255 characterss
static int count_255 = 0;
static char bufr_255[255];
static int bufr_char_255(varfl val, int ind) {
    bufr_255[count_255++] = val;
    return 1;
}

static char* FXY(dd *desc) 
{
    char* FXY_fmt = malloc(8);
    sprintf(FXY_fmt, "%i-%02i-%03i", desc->f, desc->x, desc->y);
    return FXY_fmt;
}

static int FXY_i(dd *desc) 
{
    return desc->f*100000 + desc->x*1000 + desc->y;
}

int indent = 0;

void repeat(char c , int count )
{
     for (int i = 0; i<count;i++){
        printf("%c", c);
     }
}

static int prev_same = 0;
static int curFXY;
static int prevFXY;

static int bufr_callback(varfl val, int ind)
{
    if( des[ind]->id == SEQDESC )
    {
        fprintf(stderr, "%s\n", FXY(&des[ind]->seq->d) );

        // get values
        bufrval_t * v = bufr_open_val_array();
        if (v == NULL) return 0;

        if (!bufr_parse_out(des[ind] -> seq -> del, 0, des[ind] -> seq -> nel - 1,
                bufr_val_to_global, 0)) {
            bufr_close_val_array();
            return 0;
        }

        for(int i = 0; i < des[ind]->seq->nel; i++) {
            int idx = get_index (ELDESC, &des[ind]->seq->del[i]);
            if( idx > 0) {
                fprintf(stderr, "\t%s: %s = %g\n", FXY(&des[ind]->seq->del[i]), des[idx]->el->elname, v->vals[i] );
            }
        }

        bufr_close_val_array();
    } 
    else 
    {
        curFXY = FXY_i(&des[ind]->el->d);

        // skip instant replicator descriptors
        if( (&des[ind]->el->d)->f == 1 ) {
            return 1;
        }

        // skip operator descriptors
        if( (&des[ind]->el->d)->f == 2 ) {
            return 1;
        }
        
        // avoid printing too much records if they are repeated often
        if(curFXY != prevFXY) {
            prev_same = 0;
        } else {
            prev_same++;
        }

        if( strcmp(des[ind]->el->unit, "CCITT IA5") != 0 ) {
            /* NUMERIC */
            if( prev_same < 5 ) {
                fprintf(stderr, "%s: %s = %g\n", FXY(&des[ind]->el->d), des[ind]->el->elname, val );
            } else if( prev_same == 5 ) {
                fprintf(stderr, "\t(...)\n" );
            }
        } else {
            /* STRING */
            char* text = calloc( 255, sizeof(char));
            dd * d = & (des[ind] -> el -> d);
            count_255 = 0;            
            bufr_parse_out(d, 0, 0, bufr_char_255, 0);

            for (int k = 0; k < count_255; ++k) {
                text[k] = bufr_255[k];
            }

            // print string value
            if( prev_same < 5 ) {
                fprintf(stderr, "%s: %s = [%s]\n", FXY(&des[ind]->el->d), des[ind]->el->elname, text );
            }            
        }

        prevFXY = curFXY;
    }

    return 1;
}