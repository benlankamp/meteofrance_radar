#!/bin/bash

# Get date and time
DATE=`h5dump -a /what/date latest_rainrate.h5 | grep -Poi "(20[0-9]{6})"`
TIME=`h5dump -a /what/time latest_rainrate.h5 | grep -Poi "([0-9]{6})"`
TIME=${TIME::-2}
DATETIME=${DATE}${TIME}

echo Doing ${DATETIME}...

# Conversion from HDF5 to PNG
source ~/anaconda3/etc/profile.d/conda.sh

conda activate base
python3 hdf2png.py latest_rainrate.h5 latest_rainrate.png
conda deactivate

echo Applying projection...
gdal_translate -of GTiff \
    -a_srs '+proj=stere +lat_0=90 +lon_0=0 +lat_ts=45 +ellps=WGS84 +x_0=619652.07 +y_0=5262818.34 +datum=WGS84' \
    -a_ullr -0.00405590608716011 1736000.00206818 1735999.9959441 0.00206815451383591 \
    "latest_rainrate.png" "latest_rainrate.tif"

echo Warping to web projection...
gdalwarp -q -overwrite -t_srs EPSG:3857 -of GTiff "latest_rainrate.tif" "latest_rainrate_warped.tif"

echo Applying color map...
gdal_translate -q -of PNG -ot Byte "latest_rainrate_warped.tif" "latest_rainrate_warped.png"
convert "latest_rainrate_warped.png" colormap.png -channel RGBA -clut "latest_rainrate_graded.png"

cp "latest_rainrate_graded.png" "${DATETIME}_fra_rainrate.png"
