#!/bin/bash

# Convert to ODIM HDF5
./comp2hdf5/bin/comp2hdf5 -d ./comp2hdf5/tables latest_reflectivity.bufr latest_reflectivity.h5

# Translate to image
DATE=`h5dump -a /what/date latest.h5 | grep -Poi "(20[0-9]{6})"`
TIME=`h5dump -a /what/time latest.h5 | grep -Poi "([0-9]{6})"`
TIME=${TIME::-2}
DATETIME=${DATE}${TIME}

echo Doing ${DATETIME}...

# Assign projection
echo Assigning projection...
gdal_translate \
        -q \
        -of GTiff \
        -a_nodata 255 \
		-a_srs '+proj=stere +lat_0=90 +lat_ts=45 +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +units=m +no_defs' \
        -a_ullr -619652.074 -3526818.338 913347.926 -5056818.338 \
		"HDF5:"latest_reflectivity.h5"://dataset1/data1/data" \
        "translated.tif"

# Convert to webmercator
echo Converting to webmercator...
gdalwarp -q -overwrite -t_srs EPSG:3857 -of GTiff "translated.tif" "warped.tif"

# Convert to PNG and reduce to 8 bit
echo Translate to PNG and reduce to 8 bit...
gdal_translate -q -of PNG -ot Byte "warped.tif" "translated.png"
convert translated.png -depth 8 -type TrueColor translated_8bit.png

# Apply colormap
echo Applying colormap...
convert translated_8bit.png colormap.png -channel RGBA -clut "translated_graded.png"

# Move to web folder
cp "translated_graded.png" "${DATETIME}_fra_new.png"
